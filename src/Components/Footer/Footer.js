import React, { Component } from "react";
import './Footer.scss';
import logo from '../../img/logo.svg';
import twitter from '../../img/twitter.svg';
import instagram from '../../img/instagram.svg';
import facebook from '../../img/facebook.svg';
import linkedin from '../../img/linkedin.svg';
import {Link} from 'react-router-dom'

class Footer extends Component {
    render() {
        return (
            <footer className="Header Footer">
            	<div className="Header-socialmedia__icons Footer-icons">
					<img src={twitter} alt="twitter" />
					<img src={instagram} alt="instagram" />
					<img src={facebook} alt="facebook" />
					<img src={linkedin} alt="linkedin" />
				</div>
            	<Link to ="/" className="Header-logo">
            		<img src={logo} alt="logo"/>
            	</Link>
				<div className="Header-socialmedia__phonenumber">
					<p>Call</p>
					<a href="tel:+78142332211">+38(066)123-45-67</a>
				</div>
			</footer>
        )
    }
}

export default Footer;


