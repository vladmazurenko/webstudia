import React, { Component } from "react";
import './About.scss';
import RecallForm from '../RecallForm/RecallForm';

class About extends Component {
    render() {
        return (
        	<div className="About">
				<div className="About-whatWeDo">
					<div className="About-whatWeDo_header">
						<h2>What we do</h2>
					</div>
					<p className="About-whatWeDo_text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor sed egestas urna s
						it mauris aenean pellentesque. Senectus rhoncus sollicitudin ut etiam fusce faucibus. Quam sed tin
						cidunt nec, lectus tortor vulputate et ultrices. Aliquet donec amet, at nec enim, enim vel, vitae, vi
						verra. Amet, convallis pellentesque cursus habitasse nulla cursus. Auctor id venenatis risus egestas ni
						bh hac ut tempus, vitae. Pellentesque et convallis pretium nec imperdiet. 
					</p>
				</div>
				<RecallForm />
				<div className="About-informationBlocks">
					<div className="About-informationBlocks_header">
						<h2>Start-up’s development</h2>
					</div>
					<div className="About-informationBlocks_text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor sed egestas urna 
						sit mauris aenean pellentesque. Senectus rhoncus sollicitudin ut etiam fusce faucibus.
						 Quam sed tincidunt nec, lectus tortor vulputate et ultrices. Aliquet donec amet, at nec enim, enim 
					</div>
				</div>
				<div className="About-informationBlocks">
					<div className="About-informationBlocks_header">
						<h2>Business automatation</h2>
					</div>
					<div className="About-informationBlocks_text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor sed egestas
						 urna sit mauris aenean pellentesque. Senectus rhoncus sollicitudin ut etiam fusce faucibus.
						  Quam sed tincidunt nec, lectus tortor vulputate et ultrices. Aliquet donec amet, at nec enim, enim 
					</div>
				</div>
				<div className="About-informationBlocks">
					<div className="About-informationBlocks_header">
						<h2>Software ingeneration</h2>
					</div>
					<div className="About-informationBlocks_text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor sed egestas urna sit mauris aenean pellentesque. 
						Senectus rhoncus sollicitudin ut etiam fusce faucibus. Quam sed tincidunt nec, lectus tortor vulputate et ultrices. 
						Aliquet donec amet, at nec enim, enim 
					</div>
				</div>
			</div>
        )
    }
}

export default About;