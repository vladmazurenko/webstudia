import React, {Fragment} from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Banner from '../Banner/Banner';
import About from '../About/About';
import Portfolio from '../Portfolio/Portfolio';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
  	<Fragment>
	    <Router>
	        <div>
	        	<Header />
		        <Switch>
		            <Route exact path='/' component={Banner} />
		            <Route path='/about' component={About} />
		            <Route path='/portfolio' component={Portfolio} />    
		        </Switch>
		        <Footer />
	        </div>
	    </Router>
    </Fragment>
  );
}

export default App;