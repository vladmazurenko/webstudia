import React, { Component } from "react";
import './Portfolio.scss';
import project_photo from '../../img/project_photo.jpg';

class Portfolio extends Component {
    render() {
        return (
        	<div className="Portfolio">
				<div className="Portfolio-header">
					<h2>
						Portfolio
					</h2>
				</div>
				<div className="Portfolio-grid">
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo" className="grow"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
					<div className="Portfolio-grid_project">
						<img src={project_photo} alt="project_photo"/>
					</div>
				</div>
			</div>
        )
    }
}

export default Portfolio;