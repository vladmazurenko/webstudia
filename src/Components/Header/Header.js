import React, { Component } from "react";
import './Header.scss';
import logo from '../../img/logo.svg';
import twitter from '../../img/twitter.svg';
import instagram from '../../img/instagram.svg';
import facebook from '../../img/facebook.svg';
import linkedin from '../../img/linkedin.svg';
import menu from '../../img/menu.svg';
import {Link} from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <header className="Header">
            	<Link to ="/" className="Header-logo">
            		<img src={logo} alt="logo"/>
            		<div className="Header-logo_hidden">
            			<p>
            				ebstudia
            			</p>
            		</div>
            	</Link>
				<nav>
			      	<input type="checkbox" id="check"/>
			      	<label htmlFor="check" className="checkbtn">
			        	<img src={menu} alt="menu"/>
			      	</label>
			      	<ul>
				        <li><Link to = "/about">About</Link></li>
				        <li><Link to = "/portfolio">Portfolio</Link></li>
				        <li><a href="#">Pricing</a></li>
				        <li><a href="#">Contacts</a></li>
			      	</ul>
			    </nav>
				<div className="Header-socialmedia">
					<div className="Header-socialmedia__phonenumber">
						<p>Call</p>
						<a href="tel:+78142332211">+38(066)123-45-67</a>
					</div>
					<div className="Header-socialmedia__icons">
						<img src={twitter} alt="twitter" />
						<img src={instagram} alt="instagram" />
						<img src={facebook} alt="facebook" />
						<img src={linkedin} alt="linkedin" />
					</div>
				</div>
			</header>
        )
    }
}

export default Header;