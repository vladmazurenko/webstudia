import React, { Component } from "react";
import './RecallForm.scss';

class RecallForm extends Component {
    render() {
        return (
        	<div className="RecallForm">
				<div className="RecallForm-header">
					<h2>Request a call</h2>
				</div>
				<form className="RecallForm-form">
					<div className="RecallForm-form__input">
						<input type="text" placeholder="Name" />
						<input type="tel" placeholder="Phonenumber" />
					</div>
					<input type="submit" className="main_button" value="Recall me" />
				</form>
			</div>
        )
    }
}

export default RecallForm;