import React, { Component } from "react";
import './Banner.scss';
import {Link} from 'react-router-dom'

class Banner extends Component {
    render() {
        return (
        	<div className="Banner">
				<h1 className="Banner-head">
					<div>WEB SERVICES</div>
					<div>DEVELOPMENT</div>
				</h1>
				<h2 className="Banner-head2">
					Website development company
				</h2>
				<Link to="/portfolio" className="Banner-portfolio-button">
					<span>PORTFOLIO</span>
				</Link>
			</div>
        )
    }
}

export default Banner;